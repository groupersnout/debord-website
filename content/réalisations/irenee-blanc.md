---
title: "Supprimer totalement un mur porteur au rez-de-chaussée"
description: "Suppression d'un mur porteur grâce à la mise en place d'une ferme métallique dans les étages"
date: 2020-05-05T15:05:34+02:00
draft: false
featured_image: /images/realisations/ireneeblanc/ireneeblanc06.webp
show_reading_time: true
toc: true
summary: "Il fallait supprimer totalement ce mur porteur de 7,20 m de long sans mettre de poteaux ni même laisser de tête de mur aux extrémités. Un vrai défi, car il fallait cependant soutenir les étages supérieurs."
lieu: /images/rues/rueireneeblanc.svg
nomdecode: rue Irénée-Blanc
tags:
- mur porteur
- ouverture de baie
- plancher
- ferme
---

## Enjeu

Afin de largement agrandir la pièce de vie, ce propriétaire souhaitait supprimer le mur porteur central au rez-de-chaussée. Mais la portée de ce mur de refend est grande, et l'immeuble fragile. Il fallait donc trouver une solution avec un faible encombrement, tout en veillant tout particulièrement à bien équilibrer les différentes charges pour ne pas trop solliciter certains points.

{{< figure src="/images/realisations/ireneeblanc/ireneeblanc09.webp" >}}

## Étude

Le mur de refend à supprimer au rez-de-chaussée fait plus de 7m de long, et l'immeuble comprend deux étages au-dessus de ce niveau. Étant donné la portée et les charges à reporter, la méthode classique consistant à remplacer le mur par une poutre ne fonctionne pas : en effet, il faudrait une poutre de section bien trop importante, qui serait donc très haute, rabaisserait le plafond, et annulerait toute impression d'espace.

Notre solution : la réalisation d'une "ferme de charpente", intégrée dans la hauteur du 1er étage. Beaucoup plus légère et discrète, elle s'intègre autour de la cloison de l'étage supérieur. Grâce à elle, aucune retombée au niveau du plafond du rez-de-chaussée ! La ferme possède un double rôle : elle suspend le plancher bas (entre le rez-de-chaussée et l'étage) et soutient le plancher haut (entre le 1er et le 2e) ainsi que la couverture. Chaque poutrelle métallique constituant cette charpente est de petite dimension et a un rôle bien précis, de flexion, traction, ou compression, comme expliqué dans ce schéma :

{{< figure src="/images/realisations/ireneeblanc/ireneeblanc08.webp" title="Principe de reprise schématique avec renvoi des forces : flexion, compression et traction" >}}

Mais ce n'est pas tout. Le bâtiment ayant été conforté au niveau de ses fondations, il était également important de ne pas trop modifier la répartition des charges sur ces fondations et sur le terrain. Il nous fallait en reporter une partie sur le mur de refend afin de retrouver au mieux les descentes de
charges antérieures. Pour cela, nous reportons les charges aux appuis de la ferme sur les murs extérieurs, puis nous effectuons un report partiel sur le mur de refend au niveau du sous-sol.

<div class="imgandcaption">

{{< figure src="/images/realisations/ireneeblanc/ireneeblanc10.webp" title="Côté mur pignon extérieur" class="ml0">}}

Du côté du mur pignon en façade, nous encastrons un poteau métallique sur toute la hauteur du rez-de-chaussée,
puis une reprise des forces au niveau du plancher bas du rez-de-chaussée, en renvoyant partiellement les charges sur le mur de refend du sous-sol.

</div>

<div class="imgandcaption">

De l'autre côté, l'encastrement d'un poteau était trop risqué (on avait des chances de se retrouver dans la cuisine du voisin). Au lieu de ça, nous encastrons horizontalement des poutres en béton armé au niveau des planchers, ce qui est plus sûr. La longueur de cette poutre est calculée de façon à répartir les charges sur une longueur de mur suffisante pour que la pression transmise sur les maçonneries de pierre reste faible. Au sous-sol, on retrouve une autre poutre en béton armée, mais inversée : elle récupère les charges pour en ramener une partie sur le mur de refend.

{{< figure src="/images/realisations/ireneeblanc/ireneeblanc11.webp" title="Côté mur mitoyen" class="mr0">}}

</div>

Il s'agit donc d'une réalisation assez complexe, mais qui une fois réalisée sera parfaitement sûre et invisible.

{{< figure src="/images/realisations/ireneeblanc/ireneeblanc07.webp" title="Plan-coupe de l'intervention" >}}

## Travaux

{{< figure src="/images/realisations/ireneeblanc/ireneeblanc02.webp" title="Le plafond a été ouvert le long de l'ancien mur de refend qui a été démoli" >}}

Nos renforts sont noyés dans l'épaisseur du plancher, de manière à ce qu'il n'y ait aucune retombée au rez-de-chaussée. Toutes les solives du plancher reposaient sur deux poutres-sablières en bois au droit du mur de refend. Nous doublons ces poutres à l'aide de fers [UPE](/comprendre/quel-fer-choisir-) de part et d'autre, dans lesquels viennent se raccrocher une à une les solives.

{{< figure src="/images/realisations/ireneeblanc/ireneeblanc03.webp" title="Les solives sont fixées à la poutre centrale à l'aide de fers plats. Notez la poutre en béton armé encastrée au dessous de l'appui" >}}

C'est sur cette nouvelle structure que viennent se fixer les autres éléments formant la ferme au niveau du 1er étage, ou plutôt les deux demi-fermes : une de chaque côté de la cloison. Elles font saillie d'environ 5 cm et pourront au besoin être dissimulées derrière un doublage, ou pas.

{{< figure src="/images/realisations/ireneeblanc/ireneeblanc04.webp" title="La ferme supporte le plancher bas tout en soutenant le plancher haut. On distingue les deux profilés en biais formant les arbalétriers, et les deux poinçons verticaux suspendant le plancher bas" >}}
{{< figure src="/images/realisations/ireneeblanc/ireneeblanc05.webp" title="On peut deviner l'endroit dans le mur de façade où a été encastré le poteau répartissant les charges, en bas à droite." >}}
{{< figure src="/images/realisations/ireneeblanc/ireneeblanc06.webp" title="L'ensemble des renforts une fois terminés, avant refermeture des planchers" >}}

## Budget

Montant de l'opération : **45 600 € TTC**.
