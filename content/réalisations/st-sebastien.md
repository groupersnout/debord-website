---
title: "Confortement d'un plancher bois par le dessus"
description: "Renforcement d'un plancher par le dessus pour ne pas dégrader les poutres apparentes."
date: 2020-05-11T01:17:48+02:00
draft: false
featured_image: /images/realisations/confortement/conf01.webp
show_reading_time: true
toc: true
summary: "Ce plancher ancien présentait des faiblesses et s'affaissait. Pour ne pas impacter les poutres apparentes, nous avons procédé à un renfort de la structure en intervenant par le dessus."
lieu: /images/rues/ruestsebastien.svg
nomdecode: rue Saint-Sébastien
tags:
- confortement
- plancher
- poutres apparentes
---

## Enjeu

Ce plancher ancien à structure bois apparente commençait à s'affaisser dangereusement : une solive était déjà rompue, et des répercussions étaient clairement visibles à l'étage supérieur.

En sous-face, les poutres sont apparentes : il fallait donc veiller à ne pas les impacter visuellement.

{{< figure src="/images/realisations/confortement/conf04.webp" title="Une solive, la quatrième en partant de la gauche, s'est rompue" class="fl-l doublepic mt0" >}}

{{< figure src="/images/realisations/confortement/conf03.webp" title="À l'étage, le plancher s'affaisse nettement" class="fr-l doublepic mt0" >}}

## Étude

Pour solidifier l'ensemble, nous proposons un confortement général par insertion d'une structure métallique dans l'épaisseur du plancher. Il faudra pour cela intervenir par le dessus. Ainsi, aucune saillie ne sera faite en sous-face : l'aspect du plafond ne sera pas modifié.

### Note de calcul

Les plus courageux et les plus pointus peuvent télécharger les [notes de calcul ici](/pdf/stsebastiennotedecalcul.pdf).


## Travaux

À l'étage, nous commençons par déposer le parquet existant avec soin, au droit des structures.

{{< figure src="/images/realisations/confortement/conf05.webp" title="Le parquet est retiré" >}}

Le renforcement de l’ensemble des bois se fait par l'encastrement d'une structure métallique porteuse. Des poutres principales sont insérées, puis des poutres secondaires recoupent la portée de chaque solive en bois. Ces solives bois totalement insuffisantes pour franchir une portée de 5m deviennent très confortables si elles sont soutenues au milieu.

{{< figure src="/images/realisations/confortement/conf06.webp" title="Création d'une structure porteuse métallique" >}}

Il nous faut lier cette nouvelle structure avec les bois d'origine. Pour cela, nous utilisons des tiges filetées, dont la tête sera encastrée en sous-face des solives.

{{< figure src="/images/realisations/confortement/conf08.webp" title="Les suspentes sont encastrées dans les solives" >}}

Une fois les renforts en place, il ne reste plus qu'à refermer en reconstituant la chape béton, avant la repose du parquet.

{{< figure src="/images/realisations/confortement/conf01.webp" title="Aucune trace de l'intervention au plafond." >}}

## Budget

Montant de l'opération : **28 000 € TTC** (sans le parquet).
