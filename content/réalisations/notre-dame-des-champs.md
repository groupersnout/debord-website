---
title: "Installation de WC dans des chambres de bonnes"
description: "Des toilettes individuelles et sans broyeur"
date: 2021-06-09T11:02:20+02:00
draft: false
featured_image: /images/realisations/wcchambre/nddc14.webp
show_reading_time: true
toc: true
summary: "Autrefois, les toilettes du dernier étage étaient communes. Comment installer des toilettes individuelles et leur tuyau d'évacuation encombrant dans ces anciennes chambres de bonnes ?"
lieu: /images/rues/ruenddeschamps.svg
nomdecode: rue Notre-Dame-des-Champs
tags:
- toilettes
- poutre
- renfort
---

## Enjeu

Le dernier étage des immeubles Haussmanniens (1850) était traditionnellement réservé au logement des bonnes et des domestiques. Il s’agit de petites chambres individuelles, souvent équipées d’un point d’eau, mais avec un WC commun sur le palier. Les modes de vie ayant évolué, ces chambres servent maintenant davantage à loger des étudiants, ou sont réunies pour former des logements plus confortables.

Au 21ᵉ siècle, nous aspirons à disposer de WC individuels dans notre propre logement. En effet, comme beaucoup d’entre nous l’ont vécu, déambuler dans les couloirs le soir en pyjama, rouleau de papier toilette sous le bras, pour rejoindre des WC souvent sales, malodorants ou froids (selon que l’on trouve la fenêtre ouverte ou non), semble aujourd'hui moyenâgeux, comme du temps ou les latrines donnaient sur les remparts des châteaux-forts.

Malheureusement, installer des WC dans ces logements est techniquement compliqué. En effet, lorsqu’il y a un point d’eau, les tuyaux d’évacuation sont tout petits (diamètre 32 ou 40 mm) et ne permettent pas de raccorder un WC.

La tentation est donc grande d’installer un WC broyeur, qui broie les matières, les pompe et les injecte sous pression dans n’importe quel tuyau. Le tour est joué !

Mais non.

Car très souvent, soit la pression est trop forte et les matières remontent dans l’évier du voisin, soit le broyeur tombe en panne (ce qui est fréquent sur ces appareils), le WC déborde et inonde le voisin du 5ᵉ.

Suite à de nombreuses plaintes de voisins du 5ᵉ, l’installation de WC broyeur est donc formellement interdite à Paris (article 47 du règlement sanitaire départemental).

>Notons toutefois qu’il peut y avoir des dérogations exceptionnelles, mais assorties de contraintes techniques quasi-insurmontables : **pente continue du tuyau** sans remontées, de manière à ce que les liquides puissent s’écouler librement en cas de panne ; **tuyau indépendant** affecté à un seul appareil ; demande d’autorisation spéciale avec **dossier technique précis et argumenté** ; autorisation de la **copropriété**…

Mais alors que faire ?

## Étude

Il n'y a qu'une seule solution : remplacer les petits tuyaux existants par un plus gros, de diamètre 100 mm minimum, qui acceptera de recevoir les différents WC, éviers et douches de tous nos sympathiques petits étudiants.

Mais il va bien falloir les placer quelque part, ces gros tuyaux. Nous avons trois choix :

**- Le long des fenêtres :** Ce tuyau est encombrant et doit avoir une pente minimum de 2 cm/m environ. C'est donc possible, mais cela oblige les chambres du bout à disposer les WC et la douche sur une estrade en hauteur.

**- Sous le plancher :** La pente ne serait plus un problème, mais notre voisin du 5ᵉ (encore lui) ne souhaite pas avoir un tuyau en fonte qui traverse le plafond de son séjour.

**- Dans l’épaisseur du plancher :** Au moins, là il ne gênera personne, c’est donc la solution !

{{< figure src="/images/realisations/wcchambre/nddc4.webp">}}

Seulement, le tuyau devra traverser les poutres du plancher. Il faudra donc modifier ponctuellement ces solives pour permettre le passage du tuyau, sinon tout va s'écrouler, et le voisin du 5ᵉ trouvera sûrement à y redire.

## Travaux

Le collecteur desservira six logements, les deux studios d'extrémité étant déjà raccordés directement sur les chutes principales.

{{< figure src="/images/realisations/wcchambre/nddc5.webp" title="Schéma en perspective du cheminement de notre collecteur dans le couloir avec les branchements dans les six logements. En bleu, les solives traversées">}}

Notre collecteur sera encastré sur la majeure partie de sa longueur, sauf le raccordement final passant en sous-face du plafond d’un placard et d’un WC de l’étage inférieur. Nos travaux seront réalisés depuis le dessus, sans intervention dans l’appartement du dessous, hormis pour le raccordement final du collecteur.

{{< figure src="/images/realisations/wcchambre/nddcschema01.webp" title="Vue en coupe du plancher">}}

Nous avons commencé par réaliser une saignée au sol du couloir central, en prenant soin de ne pas abimer les tomettes afin de pouvoir les replacer une fois le travail terminé. 

{{< figure src="/images/realisations/wcchambre/nddc8.webp">}}

Ensuite, il nous faut modifier les solives pour permettre le cheminement du tuyau tout en respectant la pente nécessaire. Pour cela, nous allons employer des renforts métalliques fabriquées en atelier, spécialement conçus pour cette tâche.

### Les renforts métalliques

Ces renforts métalliques nous permettent de diminuer ponctuellement la hauteur des solives, à l’endroit où passe le tuyau. Ils ont été conçus et calculés en fonction des charges effectivement supportées par ces structures de plancher.

{{< figure src="/images/realisations/wcchambre/nddcschema5.webp">}}

Ces renforts sont de deux types distincts. En effet, au départ du tuyau, en haut, il suffisait de rogner quelques centimètres sur le dessus des poutrelles pour passer notre canalisation de 10 cm de diamètre. Le renfort a donc été installé en partie basse.

{{< figure src="/images/realisations/wcchambre/nddcschema02.webp">}}

À l’arrivée, en bas, le tuyau chemine dans la partie inférieure du plancher : le renfort a alors été installé en partie supérieure.

{{< figure src="/images/realisations/wcchambre/nddcschema03.webp">}}

<div class="flex flex-wrap">

{{< figure src="/images/realisations/wcchambre/nddc6.webp" title="Renfort de type « A » pour « aval »" class="w-48-l center" >}}

{{< figure src="/images/realisations/wcchambre/nddc6-1.webp" title="Renfort de type « B » pour « amont » (« A » était déjà pris)" class="w-48-l center" >}}

</div>

{{< figure src="/images/realisations/wcchambre/nddc7.webp" title="L'ensemble de la collection">}}

### Mise en charge

Si on se contente de fixer les renforts, un léger affaissement risque de se produire au moment de découper l’encoche dans la solive, le temps que le renfort prenne le relai. On risque alors de voir apparaître des fissures au plafond du dessous. Et alors que dira le voisin du 5ᵉ ?

<div class="imgandcaption">

Pour éviter ces désordres, il suffit de [mettre en charge](/comprendre/mise-en-charge) les renforts avant d’effectuer la découpe, afin d’effectuer le transfert des contraintes de la solive vers le renfort.

{{< figure src="/images/realisations/wcchambre/nddc9.webp" title="Mise en charge à la clé dynamométrique" class="mr0">}}

</div>

{{< figure src="/images/realisations/wcchambre/nddcschema04.webp" title="Schéma de principe de la mise en charge">}}

Une fois ce travail effectué, la solive n’étant plus soumise à aucune contrainte à cet endroit, nous pouvons effectuer la découpe des encoches en toute tranquillité.

<div class="flex flex-wrap">

{{< figure src="/images/realisations/wcchambre/nddc10.webp" title="Réalisation des encoches" class="w-48-l center" >}}

{{< figure src="/images/realisations/wcchambre/nddc15.webp" title="Bloquage des renforts au béton résinique" class="w-48-l center" >}}

</div>

Et voilà, nous sommes prêts pour le passage du plombier.

<div class="flex flex-wrap">

{{< figure src="/images/realisations/wcchambre/nddc11.webp" title="Tout est prêt pour faire passer le tuyau." class="w-48-l center" >}}

{{< figure src="/images/realisations/wcchambre/nddc16.webp" title="Essai de passage des tuyaux" class="w-48-l center" >}}

</div>

Il ne reste plus qu'à reboucher la tranchée, reposer les tomettes et passer voir le voisin du 5ᵉ.

<div class="flex flex-wrap">

{{< figure src="/images/realisations/wcchambre/nddc12.webp" title="Aucune trace ne subsiste dans le couloir" class="w-48-l center" >}}

{{< figure src="/images/realisations/wcchambre/nddc17.webp" title="Ouf, aucune fissure au plafond de notre estimé voisin" class="w-48-l center" >}}

</div>

{{< figure src="/images/realisations/wcchambre/nddc01.webp" title="Il ne reste plus qu'à profiter de ces belles toilettes aux normes.">}}

## Budget

Montant de l'opération : **9 000 € TTC** (sans la plomberie ni la refermeture).
