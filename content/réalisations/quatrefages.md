---
title: "Ouverture d'un mur porteur"
description: "Réunir deux pièces en supprimant un mur porteur"
date: 2020-03-06T14:58:01+01:00
draft: false
featured_image: /images/realisations//ouverture/ouv02.webp
show_reading_time: true
toc: true
summary: "Afin de réaliser une pièce de vie confortable, une paroi entre deux petites pièces devait être supprimée. Mais ce mur étant porteur, il fallait mettre en place la structure nécessaire pour ne créer aucun désordre dans le bâtiment tout en étant le plus discret possible."
lieu: /images/rues/ruedequatrefages.svg
nomdecode: rue de Quatrefages
tags:
- ouverture de baie
- mur porteur
- poutre
---

## Enjeu

Le mur porteur à supprimer est en pan de bois, d'une vingtaines de centimètres, donc peu épais. Le défi, c'est de parvenir à ne pas dépasser cette faible épaisseur malgré les renforts, pour conserver les moulures et corniches de plafond qui ornent cet appartement. Aussi, on réalisera une poutre de très faible retombée pour obtenir l'espace d'une seule et même grande pièce.

## Étude

Dans un premier temps, nous avons effectué un relevé de la structure au différents étages du bâtiment, afin de définir précisément les descentes de charge en présence et nous permettre de dimensionner les éléments ainsi que la méthodologie de l'opération.

Lorsque cette propriétaire nous a contacté, elle avait déjà fait faire une première étude, qui recommandait l'installation d'une poutre de 45 cm de large sur 30 cm de haut, totalement incompatible avec les objectifs que nous avons atteints. **Notre solution nous a permis de diviser par deux l'encombrement, avec seulement 20 cm de large et 17 cm de haut.**

{{< animsvg src="/images/realisations/ouverture/planouverturebaie.svg" duration="800" title="Schéma de l'intervention">}}


## Travaux

### Préparation

{{< figure src="/images/realisations/ouverture/ouv00.webp" title="Mur avant ouverture" >}}

Un peu au-dessous du niveau du sol, nous commençons par réaliser une semelle de répartition encastrée grâce au scellement d'un profilé métallique [HEA](/comprendre/quel-fer-choisir-/#les-différents-types-de-fer) au plâtre à bâtir. Ce HEA de 100 mm est accompagné d'un vérin pour la mise en charge du poteau qui s'y appuiera, une précharge se fera à 5 tonnes. Le tout sera ensuite immobilisé définitivement grâce à deux joues en fer plat soudées.
Cette première mise en charge est destinée à reprendre le poids des étages supérieurs.

> La [mise en charge](/comprendre/mise-en-charge/) consiste à appliquer une précontrainte dans les profilés métalliques au moment de leur mise en œuvre, ce qui évite que des affaissements se produisent par la suite, lorsque les maçonneries « s’assoient » naturellement sur des nouvelles structures.

{{< figure src="/images/realisations/ouverture/ouverture03.webp" title="Vérin équipant le pied du poteau HEA" class="fl-l doublepic">}}

{{< figure src="/images/realisations/ouverture/ouverture02.webp" title="Liaisonnement de renfort des différents assemblages à l'aide de fers plats moisés, soudés et fixés par tirefonds." class="fr-l doublepic">}}


### Mise en place des renforts

{{< figure src="/images/realisations/ouverture/ouv01.webp" title="Sondages en cours" >}}

{{< figure src="/images/realisations/ouverture/ouverture04.webp" title="Liaison entre l'ancienne structure, la nouvelle poutre et le nouveau poteau" class="w-40-l fr-l mt0">}}

Les moulures qui décorent le plafond sont soigneusement préservées. Certaines parties sont retirées et conservées afin d'être remises en fin de chantier.

Un poteau bois sera conservé d'un côté et renforcé pour servir d'appui à notre portique.

Le second appui sera réalisé par création d'un poteau métallique. Ce poteau sera mis en place et mis en charge avant la démolition du mur. Le transfert de charge se fera donc progressivement, sans avoir recours à un étaiement important.

L'étaiement portera sur le seul plancher, de part et d'autre, avec deux batteries de trois étais, avant de passer à la démolition finale du mur pour permettre la réalisation de la poutre centrale.

La sablière haute est coupée aux dimensions finales. À la place de cette dernière, nous insérons deux fers [UPE](comprendre/quel-fer-choisir-/#les-différents-types-de-fer) de 140 mm équipés de deux cornières d'appui. La liaison avec l'ancienne structure est renforcée par moisage de fers plats.
La seconde mise en charge consistant à reprendre les charges du plancher est effectuée par calage léger sur chaque solive, à 200 daN.

> Les fers et les accessoires de fixation sont traités contre la corrosion par galvanisation, électrozingage ou peinture antirouille en deux couches.

{{< figure src="/images/realisations/ouverture/ouv02.webp" title="Renfort de structure réalisé" >}}

{{< figure src="/images/realisations/ouverture/ouv04.webp" title="Vue de l'autre côté" >}}

### Blocage définitif

Après les différentes mises en charge, nous pouvons déposer les étaiements et procéder au blocage définitif des éléments de renfort, des différentes maçonneries, des plafonds et du sol.

Il ne restera plus qu'à coffrer l'ensemble, réviser les corniches, et refaire les peintures.

{{< figure src="/images/realisations/ouverture/corniches.webp" title="Les corniches ont été conservées, la hauteur de poutre est très raisonnable." >}}

{{< figure src="/images/realisations/ouverture/ouv03.webp" title="Une belle pièce de vie !" >}}

## Budget

Montant de l'opération : **15 000 € TTC** (sans les plâtres).
