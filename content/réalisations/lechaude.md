---
title: "Ouverture et modification d'un mur de refend"
description: "Renfort d'un mur de refend en pan de bois"
date: 2020-03-01T05:40:02+01:00
draft: false
featured_image: /images/realisations/refend/ouverturerefend01.webp
show_reading_time: true
toc: true
summary: "Afin de pouvoir mettre en apparent la structure bois de ce mur porteur, il fallait trouver une solution pour rester stable en cas d'incendie."
lieu: /images/rues/ruedelechaude.svg
nomdecode: rue de L'Échaudé
tags:
- pan de bois
- mur porteur
- poutre
- ouverture de baie
---

## Enjeu

Dans ce logement, le propriétaire souhaitait mettre en apparent non seulement les poutraisons en plafond, mais aussi la structure bois d'un mur en supprimant les remplissages et quelques poteaux. Or, les poteaux, potelets et autres tournisses ont une section trop faible pour résister une heure sans protection à un incendie.  
Il fallait donc trouver une solution discrète mais sûre pour renforcer l'ensemble.

{{< figure src="/images/realisations/refend/ouverture_modification_mur_de_refend2.webp" title="Vue en coupe de l'état initial" >}}

## Étude

Après avoir effectué un relevé précis sur place, nous avons proposé de modifier les deux poutres sablières du plancher haut pour n'en faire qu'une, de 50cm de haut, apte à franchir toute la pièce. Le mur ne serait donc plus porteur et pourrait être modifié sans problème. Il conservera néanmoins un rôle secondaire de stabilité horizontale, en participant au contreventement général.

{{< figure src="/images/realisations/refend/ouverture_modification_mur_de_refend3.webp" title="Vue en coupe de l'intervention" >}}

Pour cela, il faut associer ces deux sablières à la réalisation d'une poutre composite de grande portée à l'aide de profilés métalliques, de plots béton et de tiges filetées. Cette modification permettra de garantir la stabilité au feu de l'immeuble.

## Travaux

### Création d'une poutre composite

Nous commençons par ouvrir deux saignées dans l'aire en plâtre par le dessus, puis par dégager les remplissages au droit des plots béton. Nous procédons ensuite au tracé et à la prise de repères angulaires pour pouvoir, à l'étage, placer les profilés métalliques et les tiges filetées. Dans l'espace entre les deux poutres sablières (l'endroit où reposent les solives), nous coulons des plots béton en prenant soin de ménager un retrait pour l'enduit de finition en sous-face du plancher. En conjuguant tous ces éléments, nous avons créé une poutre composite de grande portée, mais en toute discrétion.

{{< figure src="/images/realisations/refend/ouverture_modification_mur_de_refend4.webp" title="Vue en coupe de la poutre composite" >}}

### Mise en place des poteaux

Aux deux extrémités du mur, nous posons deux poteaux métalliques pour renforcer la façade et soutenir la poutre. Nous procédons à une [mise en charge](/comprendre/mise-en-charge) de l'ensemble. À présent, le mur n'est plus porteur : les différents poteaux en bois peuvent être modifiés ou supprimés à la demande.

{{< figure src="/images/realisations/refend/refend01.webp" title="Le mur n'est plus porteur" >}}

### Protections coupe-feu

Il reste ensuite à façonner les protections coupe-feu des planchers. Pour rendre les poutres apparentes, nous avons calculé qu'en protégeant suffisamment sur la hauteur les solives,  leur section permettait de résister correctement au feu. Nous refermons donc les entrevous (l'espace entre les solives) à l'aide d'un treillis galvanisé type Nergalto sur lequel nous réalisons un enduit plâtre traditionnel.

{{< figure src="/images/realisations/refend/refend02.webp" title="Le treillis type Nergalto permet l'accroche d'un enduit plâtre traditionnel" >}}

Les poteaux métalliques sont quant à eux protégés complètement par un coffrage en plâtre.

{{< figure src="/images/realisations/refend/ouverturerefend01.webp" title="La pièce une fois le plâtre terminé" >}}

## Budget

Montant de l'opération : **17 000 € TTC**.
