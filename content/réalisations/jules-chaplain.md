---
title: "Comment faire une encoche dans une grosse poutre"
description: "Modifier une grosse poutre de façon à faire passer un ascenseur"
date: 2020-05-25T14:41:45+02:00
draft: false
featured_image: /images/realisations/juleschaplain/chaplain07.webp
show_reading_time: true
toc: true
summary: "Une poutre maîtresse gênait l'installation d'un ascenseur dans un immeuble. Il a donc fallu trouver un moyen de la modifier sans risque."
lieu: /images/rues/ruejuleschaplain.svg
nomdecode: rue Jules-Chaplain
tags:
- encoche
- poutre
---

## Enjeu

{{< figure src="/images/realisations/juleschaplain/chaplain01.webp" class="w-50-l fl-l mt0 ml0" >}}

Les propriétaires de cet immeuble souhaitaient installer un ascenseur dans la cage d'escalier. Seulement, au dernier étage, une grosse poutre métallique gêne le passage en raison de sa hauteur. Il fallait donc trouver une solution pour découper une encoche, mais sans affecter les capacités porteuses de cette poutre.

## Étude

{{< figure src="/images/realisations/juleschaplain/chaplain03.webp" title="La poutre métallique de 40cm de haut a une retombée trop importante pour le passage d'un ascenseur">}}

L'installation de l'ascenseur nécessite environ 17 cm de plus : la poutre doit donc être réduite de près de la moitié de sa hauteur. Cette poutre a été installée au siècle dernier pour suspendre les deux étages supérieurs qui ont été rajoutés : les charges qu'elle supporte sont très importantes.

Notre solution consiste à la renforcer en solidarisant deux poutres plus petites mais plus larges de part et d'autre, pour remplacer la capacité porteuse qui sera dégradée par l'encoche. Cette solution est peu destructive, puisque nous ne touchons ni aux appartements ni aux murs. Nous limitons l'intervention à la seule cage d'escalier.

## Travaux

De part et d'autre de la poutre à modifier, façonnage de deux poutres de renfort, plus petites.

{{< figure src="/images/realisations/juleschaplain/chaplain04.webp" title="Mise en place des renforts et du système de mise en charge">}}

Ensuite, nous effectuons une [mise en charge](/comprendre/mise-en-charge) de ces renforts. Cela permet de transférer les efforts existants dans la poutre sur les renforts afin de s'assurer qu'il n'y aura aucun mouvement dans la structure lors de la découpe de l'encoche. La mise en charge est effectuée à la [clé dynamométrique](/comprendre/mise-en-charge/#mesure-au-dynamomètre-de-pied) selon une force préalablement calculée et mesurée sur le chantier.

{{< figure src="/images/realisations/juleschaplain/chaplain05.webp" title="Mise en charge à la clé dynamométrique, ou ici au peson" >}}

Nos renforts sont à présent prêts à être solidarisés à la poutre existante. Ils reçoivent ensuite un traitement anti-corrosion.

{{< figure src="/images/realisations/juleschaplain/chaplain06.webp" title="Les renforts sont solidarisés avec la poutre existante">}}

Maintenant que la poutre est renforcée, nous pouvons sans risque découper une encoche en son centre.

{{< figure src="/images/realisations/juleschaplain/chaplain08.webp" title="L'encoche a été découpée">}}

Pour finir, nous réalisons un coffrage autour de la poutre renforcée. Ce n'est pas seulement esthétique : le coffrage en plâtre possède un rôle important de coupe-feu permettant de s'assurer que les structures resteront stables en cas d'incendie (un effondrement serait alors encore plus désagréable).

{{< figure src="/images/realisations/juleschaplain/chaplain07.webp" title="La poutre renforcée est protégée par un coffrage en plâtre coupe-feu">}}

Tout est prêt, vous pouvez ramener l'ascenseur !

{{< figure src="/images/realisations/juleschaplain/chaplain02.webp" title="L'ascenseur une fois mis en place">}}

## Budget

Montant de l'opération : **13 800 € TTC**.
