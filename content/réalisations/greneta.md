---
title: "Confortement d'un plancher à poutres apparentes par le dessous"
description: "Renforcement de plancher en conservant l'apparence authentique."
date: 2020-05-11T01:19:36+02:00
draft: false
featured_image: /images/realisations/poutraison/pout09.webp
show_reading_time: true
toc: true
summary: "Surchargée par le poids des étages supérieurs, cette structure bois apparente s’est rompue à plusieurs endroits. S’agissant d’une boutique dans un vieil immeuble, l’apparence authentique et la hauteur sous plafond devaient être conservées."
lieu: /images/rues/ruegreneta.svg
nomdecode: rue Greneta
tags:
- plancher
- confortement
- poutres apparentes
---

## Enjeu

{{< figure src="/images/realisations/poutraison/pout01.webp">}}

Dans un vieil immeuble du quartier « Bonne Nouvelle », le plancher haut de cette boutique s'affaissait. Il nous fallait donc le conforter en gardant l'apparence authentique des lieux, notamment les poutres apparentes.

{{< figure src="/images/realisations/poutraison/pout03.webp">}}

## Étude

Ce plancher était surchargé par le poids des étages supérieurs, sa structure bois s’est même rompue à plusieurs endroits. La hauteur sous plafond est relativement faible.

L’ensemble est constitué de quatre solives porteuses principales, dix chevêtres, vingt-huit solives courantes ou boiteuses. Les entrevous sont en enduit plâtre sur lattis de châtaigner. Quant au revêtement supérieur, il est constitué de tomettes.

Chaque élément existant a été vérifié et recalculé, en vue d'être renforcé en fonction des capacités portantes des bois, des charges à reprendre et des normes actuelles de résistance aux surcharges d'exploitation.

Afin de préserver au maximum les poutres apparentes, nos renforts métalliques seront soit encastrés dans le bois, soit insérés dans l'épaisseur du plancher en partie supérieure, afin de pouvoir être dissimulés et protégés.


## Travaux

{{< figure src="/images/realisations/poutraison/pout02.webp">}}

Nous renforçons les solives porteuses à l'aide de profilés métalliques de hauteur réduite de type [UPE](/comprendre/quel-fer-choisir-) de 100 et de 80mm. Des renforts complémentaires au droit des assemblages sont réalisés par encastrement de fers à béton type TOR scellés à la résine.
Les autres solives sont pour leur part doublées par des tubes carrés de chaque côté.

{{< figure src="/images/realisations/poutraison/pout04.webp" title="Mise en place des renforts" >}}

{{< figure src="/images/realisations/poutraison/pout06.webp" title="La structure métallique est constituée de fers UPE et de tubes carrés" >}}

Les fers sont dimensionnés et placés de manière à réserver environ 10 cm de retrait par rapport de la sous-face des bois. Ainsi, ils pourront être intégrés aux entrevous plâtre et laisser les bois majoritairement apparents.

>Les bois trop affaiblis ou trop sollicités sont renforcés en y encastrant des fers TOR en sous-face.

{{< figure src="/images/realisations/poutraison/pout07.webp" title="Les entrevous au plâtre en cours de réalisation. À noter, l'encastrement de fers TOR sur la poutre de gauche et le chevêtre au fond" >}}

Les entrevous en plâtre traditionnel sont réalisés sur un treillis métallique type Nergalto. En plus de cacher les renforts, cela permet d'offrir une protection au feu : assurer la stabilité des structures en cas d’incendie et le coupe-feu avec les locaux du 1er étage. De plus, l’isolation phonique en est renforcée.

{{< figure src="/images/realisations/poutraison/pout09.webp" >}}

{{< figure src="/images/realisations/poutraison/pout08.webp" class="fl-l doublepic" >}}

{{< figure src="/images/realisations/poutraison/pout10.webp" class="fr-l doublepic" >}}

## Budget

Montant de l'opération : **68 000 € TTC** (avec les plâtres).
