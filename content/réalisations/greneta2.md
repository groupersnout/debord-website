---
title: "Réparation d'une poutre"
description: "Réparation d'une poutre rompue avec fers encastrés et sous-tendus"
date: 2021-06-09T11:01:24+02:00
draft: false
featured_image: /images/realisations/greneta2/greneta2-20.webp
show_reading_time: true
toc: true
summary: "Une poutre ancienne supportait un poids trop important et s'affaissait jusqu'à la rupture. Le plafond remarquablement décoré nécessitait de trouver une solution discrète pour consolider l'ensemble."
lieu: /images/rues/ruegreneta.svg
nomdecode: rue Greneta 2
tags:
- poutre
- réparation
---

## Enjeu

{{< figure src="/images/realisations/greneta2/greneta2-01.webp">}}

La grosse poutre du séjour a subi un affaissement important, de 15 cm en son centre, et s'est rompue.

<div class="flex flex-wrap">

{{< figure src="/images/realisations/greneta2/greneta2-02.webp" class="w-48-l center" >}}

{{< figure src="/images/realisations/greneta2/greneta2-03.webp" title="La poutre s'est cassée en son centre." class="w-48-l center" >}}

</div>

Il faut donc la consolider, mais le plafond possède une poutraison apparente du 17ᵉ siècle, avec une décoration remarquable datant de 1640 - 1650.

{{< figure src="/images/realisations/greneta2/greneta2-04.webp">}}

La solution à mettre en œuvre doit donc être la plus discrète possible pour ne pas porter atteinte à l'esthétique des lieux.

## Étude

Cette grosse poutre centrale a une portée libre supérieure à 6 m, sans cloison. Elle est de section relativement importante, cependant la poutre équivalente à l’étage supérieur est plus petite, insuffisante, et [se repose donc sur les cloisons](/comprendre/cloison-porteuse) présentes au 3ᵉ étage. 

Par conséquent, notre poutre doit recevoir les charges de deux niveaux de plancher, ce qu’elle n’a pas supporté.

Dans ce genre de cas, les solutions les plus simples sont :
- Doublement de la poutre par moisage de profilés en acier
- Remplacement par une poutre neuve

Mais évidemment, ces deux solutions auraient détruit l’authenticité de ce plafond.

Nous  avons donc proposé de conserver cette poutre et de la réparer en lui gardant son apparence et l’aspect esthétique de la pièce. Pour cela, le choix retenu a été d'utiliser des tirants en acier encastrés dans la partie inférieure de la poutre puis mis en tension par dilatation-rétractation thermique.

{{< figure src="/images/realisations/greneta2/greneta2-21.webp" title="Schéma de principe de reprise">}}

Avant cela, nous avons commencé par diminuer l'affaissement, qui était trop important.

### Note de calcul

Les plus courageux et les plus pointus peuvent télécharger les [notes de calcul ici](/pdf/grenetanotedecalcul.pdf).

## Travaux

Avant toute chose, nous avons procédé à l'étaiement de tous les étages inférieurs, jusqu'au sous-sol.

<div class="flex flex-wrap">

{{< figure src="/images/realisations/greneta2/greneta2-06.webp" class="w-48-l center" >}}

{{< figure src="/images/realisations/greneta2/greneta2-05.webp" class="w-48-l center" >}}

</div>

### Rehaussement

Lors d'une première phase préparatoire, nous avons procédé au rehaussement de la poutre. En effet, elle s'était déjà affaissée de 15 cm, ce qui était trop important pour être conservé.

Ce rehaussement a été fait à l’aide d’un vérin hydraulique d'une capacité de 20 tonnes, en mesurant la contrainte et la déformation. Nous avons appliqué les 15 tonnes correspondant au poids propre de nos deux planchers. Le contrôle de la force s'effectue au peson, et celui du déplacement au laser.

{{< figure src="/images/realisations/greneta2/greneta2-07.webp" title="Le vérin hydraulique va progressivement réhausser la poutre.">}}

Cette opération a été réalisée progressivement lors de trois interventions successives, espacées de plusieurs semaines. Au final, ce travail a abouti à un rehaussement de 10 cm environ.

### Pose des tirants encastrés

Nous réalisons des saignées en partie inférieure de la poutre afin de pouvoir y insérer les fers.

{{< figure src="/images/realisations/greneta2/greneta2-09.webp">}}

{{< figure src="/images/realisations/greneta2/greneta2-10.webp">}}

Six tirants composés de fers Tors de 20 mm sont ensuite préparés, puis encastrés.

<div class="flex flex-wrap">

{{< figure src="/images/realisations/greneta2/greneta2-12.webp" title="Installation des isolateurs" class="w-48-l center" >}}

{{< figure src="/images/realisations/greneta2/greneta2-14.webp" class="w-48-l center" >}}

</div>

{{< figure src="/images/realisations/greneta2/greneta2-13.webp">}}

### Mise en charge

Afin d'éviter de futurs mouvements et fissures, nous procédons à une [mise en charge](/comprendre/mise-en-charge).

Il existe plusieurs techniques pour cela, dont beaucoup sont détaillées [sur cette page](/comprendre/les-techniques-de-mise-en-charge). Comme souvent, le choix de la technique employée est dictée par les contraintes environnantes.

Ici, les extrémités de la poutre n’étant pas accessibles, il n’a pas été possible de mettre en tension les fers à l’aide de croix de Saint André par exemple. Nous avons donc opté pour une mise en charge par dilatation/rétractation des fers.

{{< figure src="/images/realisations/greneta2/greneta2-15.webp">}}

Une fois en place, les fers ont été chauffés à 120° C (soit + 100° C de l'ambiance) à l’aide d’un courant électrique très basse tension.

<div class="flex flex-wrap">

{{< figure src="/images/realisations/greneta2/greneta2-16.webp" title="À noter, l'isolation de la zone de travail" class="w-48-l center" >}}

{{< figure src="/images/realisations/greneta2/greneta2-17.webp" title="La température est contrôlée de façon précise" class="w-48-l center" >}}

</div>

> Le point d'auto-inflammation du bois se situe à 400° C, et celui du papier et de la sciure à 233° C. Il est donc très important de veiller à ne pas atteindre de telles températures !

Ensuite, les fers sont scellés à leurs extrémités dans un premier temps, puis sur toute leur longueur.

{{< figure src="/images/realisations/greneta2/greneta2-18.webp" title="Les fers sont scellés à la poutre par injection de résine">}}

{{< figure src="/images/realisations/greneta2/greneta2-19.webp" title="Pour parfaire l'esthétique, les saignées sont rebouchées à l'aide de baguettes en chêne avec raccord en mastic bois">}}

Lors du refroidissement, la rétractation du métal reproduit l’effort calculé, stabilisant ainsi la poutre au niveau choisi. La [flèche](/comprendre/mise-en-charge/#éléments-horizontaux) totale stabilisée est de 85 mm.

{{< figure src="/images/realisations/greneta2/greneta2-20.webp">}}

## Budget

Montant de l'opération : **16 600 € TTC**.
