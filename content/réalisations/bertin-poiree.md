---
title: "Reprise d'un mur pignon"
description: "Confortement d'un mur de façade trop faible"
date: 2020-03-01T05:40:02+01:00
draft: false
featured_image: /images/realisations/bertinpoiree/reprisepignon17.webp
show_reading_time: true
toc: true
summary: "Le mur de pignon de cet immeuble fissurait et s'affaissait dangereusement. Il fallait le renforcer et s'assurer de sa solidarité avec le reste du bâtiment."
lieu: /images/rues/ruebertinpoiree.svg
nomdecode: rue Bertin-Poirée
tags:
- façade
- confortement
- chaînage
---


## Enjeu

L'ensemble d'un mur de façade présentait des fissurations et des gonflements trahissant des mouvements structurels pouvant devenir dangereux : un effondrement était à craindre.

Il s'agissait donc de renforcer l'ensemble de la maçonnerie, sans pour autant recourir à une démolition / réfection totale qui aurait obligé à fermer l'hotel pendant plusieurs mois.


## Étude

Nous avons commencé par faire piocher les enduits extérieurs afin de comprendre exactement l'étendue et la nature du problème. Ceci nous a permis de faire un certain nombre d'observations :

<div class="imgandcaption">

{{< figure src="/images/realisations/bertinpoiree/reprisepignon01.webp" title="Relevé de l'état structurel du pignon. La partie droite correspond à un petit retour, le bâtiment suivant étant en retrait" class="ma0">}}

- L'affaissement est centré sur la droite du mur et sur le haut du rez-de-chaussée. Un gonflement du mur est perceptible à cet endroit.
- Le mur extérieur commence à se décoller des planchers d'étages, ce qui est visible dans les chambres.
- Les maçonneries en moellons sont d'une épaisseur anormalement faible (36 cm) et victimes d'une déstructuration généralisée.
- Quelques segments de chaînages métalliques sont présents (les traits rouges sur le schéma), mais aucun chainage général horizontal ni tirants faisant la liaison entre le mur et le reste du bâtiment.
- Au niveau de l'angle gauche, entre la rue Bertin-Poirée et la rue des Deux Boules, se trouve un appareil de pierres taillées de bonne facture. On retrouve la même chose à peu près au milieu du mur. Il n'y a par contre rien côté droit, ce qui expliquerait l'affaissement.

</div>

>Nous pouvons déduire de tout ça que ce mur a probablement dû faire l'objet de transformations liées à des travaux d'urbanisme tels que la modification du tracé de la rue.

{{< figure src="/images/realisations/bertinpoiree/reprisepignon03.webp" title="Quelques segments de chaînages métalliques sont présent sur cette façade fissurée">}}

## Travaux

### Mise en sécurité

<div class="flex flex-wrap">

{{< figure src="/images/realisations/bertinpoiree/reprisepignon02.webp" class="w-48-l center" >}}

{{< figure src="/images/realisations/bertinpoiree/reprisepignon04.webp" class="w-48-l center" >}}

</div>


Dans un premier temps, nous devons mettre en sécurité ce mur afin de pouvoir intervenir sans risque d'effondrement. Pour cela, nous commençons par effectuer un chaînage provisoire du bâtiment, en applique.

>Afin de sceller ce chaînage, nous devons creuser des ancres. Le mur étant fragile, il faut faire ces forages au trépan diamant ou au foret sans percussion, pour éviter les vibrations.

Ensuite, nous installons quatre poteaux métalliques provisoires sur les deux premiers niveaux, permettant le démontage en sécurité des murs au droit des futurs poteaux.

<div class="flex flex-wrap">

{{< figure src="/images/realisations/bertinpoiree/reprisepignon06.webp" title="Mise en place d'un chaînage provisoire et de poteaux" class="w-48-l center">}}

{{< figure src="/images/realisations/bertinpoiree/reprisepignon05.webp" title="Vue du chaînage et des poteaux provisoires" class="w-48-l center">}}

</div>

Des tirants sont insérés dans les planchers bas du 1er et du 2e étage, perpendiculairement et au centre du mur. Ils sont fixés dans un premier temps à nos poteaux provisoires, mais seront ensuite repris sur les chaînages définitifs. Afin de gêner le moins longtemps possible, les sols sont immédiatement refaits et les revêtements reposés avant d'aller plus loin dans le chantier.

<div class="flex justify-center flex-wrap">

  {{< figure src="/images/realisations/bertinpoiree/reprisepignon13.webp" title="Insertion dans le sol" class="ma1 w-30-l w-40-m">}}
  {{< figure src="/images/realisations/bertinpoiree/reprisepignon14.webp" title="Passage dans un placard" class="ma1 w-30-l w-40-m">}}
  {{< figure src="/images/realisations/bertinpoiree/reprisepignon15.webp" title="Passage à travers des gaines électriques sans coupure" class="ma1 w-30-l w-40-m">}}

</div>

### Confortement par poteaux-raidisseurs

Le confortement du mur se fait à l'aide de deux dispositions qui, additionnées, pourront garantir sa cohésion et sa solidité : poteaux et chaînages.

{{< figure src="/images/realisations/bertinpoiree/reprisepignon07.webp" title="Schéma des éléments de confortement : poteaux en béton armé en gris, chaînage en rouge" class="ma5-l">}}

En bas, sur environ 6 mètres de haut (soit les deux premiers niveaux), nous réalisons deux poteaux-raidisseurs en béton armé encastrés dans le mur. Cela correspond aux parties les plus endommagées et les plus chargées, puisque tout le reste repose dessus.

Afin d'insérer ces poteaux, qui reposeront sur une assise répartissant les charges sur les murs du sous-sol, nous démontons environ deux tiers de l'épaisseur du mur. Pour limiter les risques d'effondrement, nous attendons que le premier poteau soit en place avant de commencer le deuxième.

<div class="flex flex-wrap">

{{< figure src="/images/realisations/bertinpoiree/reprisepignon08.webp" title="Ouverture d'une saignée pour encastrement du poteau" class="w-48-l center">}}

{{< figure src="/images/realisations/bertinpoiree/reprisepignon09.webp" title="Armature en fer en place avant coulage du béton" class="w-48-l center">}}

</div>

>À cette occasion nous avons découvert une ancienne fenêtre au rez-de-chaussée, qu'il a fallu contourner légèrement.


Les poteaux en béton armé sont coulés en place, directement entre les pierres, pour une parfaite liaison avec les moellons existants. Il faut prendre soin d'avancer par petites hauteurs d'environ 1,50 mètre, pour éviter les désordres dus à la pression du béton frais.

{{< figure src="/images/realisations/bertinpoiree/reprisepignon12.webp" title="Le béton est coulé sur place, petit à petit" class="w-50-l center">}}

Enfin, chaque poteau est coiffé d'un chapiteau pour recevoir les charges des étages supérieurs.

>Notons que malgré l'importance des travaux, aucun désordre intérieur n'a été signalé.

### Confortement par chaînages

Une fois les poteaux-raidisseurs réalisés, nous passons au chaînage, c'est-à-dire à un cerclage horizontal du mur à l'aide de fers plats. Ceux-ci sont placés au niveau du plancher de chaque étage.

Après avoir créé les ancrages, nous ouvrons des saignées pour l'encastrement de ces fers de chainage.

Il faut ensuite liaisonner et reprendre les tirants mis en place au début du chantier sur les chaînages et poteaux extérieurs. Il ne reste plus qu'à sceller et bloquer définitivement l'ensemble, puis à retirer nos étaiements et chaînages provisoires.

Maintenant, c'est à l'entreprise de ravalement de jouer !

<div class="flex flex-wrap">

  {{< figure src="/images/realisations/bertinpoiree/reprisepignon17.webp" class="w-48-l center">}}

  {{< figure src="/images/realisations/bertinpoiree/reprisepignon18.webp" class="w-48-l center ">}}

</div>

## Budget

Montant de l'opération : **93 000 € TTC** (compris étaiements, sans finitions, TVA à 20 %).
