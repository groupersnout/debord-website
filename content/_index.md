---
title: "Accueil"
date: 2020-04-02T23:18:05+02:00
draft: false
---

#### Réparation, modification et renforcement des structures anciennes, avec reprise en sous-œuvre


{{< figure src="/images/photoaccueil.webp" class="w-60-ns center">}}


Notre entreprise est spécialisée dans le confortement des éléments porteurs des constructions anciennes (murs, poutres, charpentes et planchers).

Nous intervenons sur Paris et ses communes limitrophes.

Nous sommes à votre disposition avec toutes les compétences nécessaires :

- Connaissance parfaite des techniques de construction du bâti parisien.
- Maîtrise de l’ensemble des techniques d’interventions sur les structures en place.
- Longue expérience d’intervention en ville et en milieu habité.

Ce savoir-faire nous permet de mettre en œuvre la technique la plus appropriée.
Nos équipes exécuteront l'ouvrage sans risque, avec un minimum de gêne et d’aléas, dans un délai court et pour un prix juste et garanti.

Nos techniques de façonnage sur chantier permettent une parfaite adaptation aux existants ainsi qu'un délai d'intervention très court.

Ce site vous permet de découvrir le savoir-faire de notre [entreprise centenaire](/lentreprise/).

<div class="mt5 bg-white appel">

# Quand faire appel à nous ?

##### Si votre immeuble se fissure
##### Si votre plancher s’affaisse dangereusement
##### Si vous souhaitez ouvrir ou supprimer un mur porteur
##### Ou encore si vous voulez ouvrir une trémie d’escalier pour faire communiquer deux niveaux

{{< button src="/contact" msg="N'hésitez pas à nous contacter !">}}

</div>

# Explorez nos réalisations

<div class="flex">

<div class="photowall w-30">
  <img src="/images/essai4.webp">
</div>

<div class="pt4 center">

Confortement de plancher, ouverture de mur porteur, reprise de mur de façade… Chaque problématique a sa solution, et souvent même plusieurs. Notre rôle, c'est de vous proposer celle qui convient le mieux à la situation et à vos besoins.

Grâce aux exemples de chantiers que vous trouverez ici – chacun avec ses spécificités –, vous aurez un aperçu de ce qu'il est possible de faire. Chaque chantier est expliqué en détail, de l'étude aux travaux proprement dits, schémas et photos à l'appui.

{{< button src="/realisations" msg="Voir toutes nos réalisations" >}}

</div>

</div>

# Des articles pour mieux comprendre

<div class="flex items-center">

<div class="w-30 pt5 pl5-l photowall">
  <img src="/images/Bordy1.webp">
</div>

<div class="pt4 center">

Nous avons toujours eu à cœur de partager et d'expliquer ce que nous faisons. Mais dans notre spécialité, il y a parfois des notions méconnues, complexes, et pourtant essentielles pour bien comprendre ce qui est en jeu. Des choses qui font la différence entre une mauvaise solution, une bonne solution et une meilleure solution.

C'est pourquoi nous proposons également des articles plus théoriques (mais abordables) sur des sujets variés. La collection s'enrichira au fur et à mesure.

{{< button src="/comprendre" msg="Découvrir tous les articles de la rubrique">}}

</div>

</div>
