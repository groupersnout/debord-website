---
title: "Comprendre"
date: 2020-03-16T18:05:07+01:00
draft: false
weight: 3
---

#### Chez Debord, nous sommes passionnés par ce que nous faisons.

#### C'est pourquoi nous vous proposons une série d'articles vous permettant de mieux comprendre un tas de choses liées à notre métier : techniques, concepts, vocabulaire, enjeux…
