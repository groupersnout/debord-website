---
title: "La mise en charge"
description: "Comprendre les mises en charge, leurs utilités, les avantages et inconvénients des différentes mises en œuvre"
date: 2020-07-01T18:52:37+01:00
draft: false
toc: true
featured_image: "/images/comprendre/Bordy - la mise en charge.webp"
show_reading_time: true
summary: "La mise en charge est une notion fondamentale pour les travaux de structure. Car saviez-vous qu'un bâtiment, c'est visqueux ?"
categories:
- mise en charge
- notions
---

## Quelques notions

Avant d'aborder les questions de mise en charge, il faut comprendre un certain nombre de notions, comme la stabilité, la viscosité ou la reprise en sous-œuvre.

### La viscosité des bâtiments

Il faut savoir que toute construction possède une certaine « viscosité » : il se déforme doucement tant qu'il n'est pas en équilibre.

Comme on le constate, les désordres n’apparaissent pas immédiatement dans un bâtiment. Ils peuvent apparaître en quelques mois, quelques années, ou même évoluer sur des décennies. Les mouvements se font en général lentement, d’où la notion de « viscosité » du bâtiment.

>Ceci se remarque de manière encore plus flagrante dans les bâtiments anciens, en pierre ou en bois, qui sont parfois tout déformés.

{{< figure src="/images/comprendre/tordu.webp" title="Non, les anciens ne construisaient pas tordu !" class="w-50-l center">}}

### Stabilité d’un bâtiment

Entre la stabilité d’une construction et son écroulement, il existe une infinité d’états d'instabilité. Si les fissures progressent, que les portes coincent malgré une mise en jeu, on parle alors d’évolution des désordres. Dans ce cas, le bâtiment est considéré comme instable.

Si on supprime une partie de mur porteur sans mettre en place de renfort, le bâtiment ne va pas s’écrouler immédiatement. En fonction de la taille de l’ouverture, les dégradations apparaitront plus ou moins vite.

- Pour une petite ouverture, elles n’apparaîtront peut-être jamais, mais une fragilité aura été créée.
- Si l’ouverture est plus importante, les dégradations apparaîtront dans les étages supérieurs avec des fissures progressant sur plusieurs années.
- Si l’ouverture est très grande, un effondrement pourra se produire dans l’heure ou les jours qui suivent.

Les bâtiments tordus se trouvent entre ces états extrêmes, c'est-à-dire qu’en un point au moins, la construction n’est pas assez solide. Ce point supportant trop de charge va alors s’écraser et descendre petit à petit, soit jusqu’à trouver une nouvelle stabilité, soit pour aller vers sa ruine si l’évolution persiste.

Ce point trop faible, origine de l’instabilité, peut avoir trois causes principales :

- Une structure trop faible dès la construction.
- Un accident, comme le pourrissement d'un bois ou une rupture de fondation suite à une fuite d'eau.
- Une modification ou une réparation du bâtiment mal calculée.

Il faut donc effectuer une reprise en sous-œuvre.

### Une reprise en sous-œuvre, c'est quoi ?

On parle de reprise en sous-œuvre lorsqu'on modifie ou qu'on répare des éléments porteurs d’un bâtiment par le dessous, tout en maintenant en place des parties de construction, de maçonnerie, de charpente ou des planchers qui sont au-dessus, avec toutes leurs charges et aménagements existants.

Les renforts à mettre en œuvre doivent donc être calculés pour reprendre et tenir la charge de ces éléments. On doit intégrer dans ce calcul le poids des charges existantes et des charges d’exploitation, auquel on ajoute des coefficients de sécurité selon les normes en vigueur.
La difficulté majeure d’une opération de reprise en sous-œuvre est de faire en sorte que rien ne bouge pendant l’opération.

Seule solution : équilibrer les charges pour atteindre la stabilité et figer cette viscosité.

>#### Et une remise en place ?
>Une fois le bâtiment penché ou tordu, la remise en place est possible mais complexe et très peu pratiquée.
Remonter tout un bâtiment affaissé est assez simple, mais lorsque c’est une partie du bâtiment qui s’est déformée, c’est plus complexe.
Le mouvement a souvent mis des années à se faire, il faudra donc des mois ou des années pour l'inverser sans tout casser. Bien sûr, plus on forcera dans l’autre sens, plus ça ira vite, mais il ne faut pas dépasser une certaine limite de « viscosité ». Sinon, on provoque l’éclatement des maçonneries ou des carrelages (Nous avons testé ces limites).

>Dans la pratique, il faudra un système d’étaiement avec vérin à charge contrôlée, à remonter régulièrement, toutes les semaines ou tous les mois, mais cela n’empêche pas totalement les fissurations et désordres qu’il faudra reprendre. Pour cette raison, il est donc souvent difficile, aléatoire, rare et coûteux de redresser totalement une partie de bâtiment ou un plancher tordu. Par contre, nous arrivons en général à les remonter d'un tiers de l'affaissement constaté environ.

Lorsque l’on fait une reprise en sous-œuvre, il est généralement admis qu’il est en pratique impossible que rien ne bouge pendant l’opération et que l’apparition de fissures est quelque-chose de « normal ». (Pour en savoir plus, vous pouvez consulter [cet article](/comprendre/cloison-porteuse/#responsabilité)).

Nous pouvons cependant limiter ce risque en effectuant des mises en charge.

## Principe de la mise en charge

Il s’agit de « forcer » le nouvel élément porteur, selon une pression équivalente au poids des constructions qu’il sera amené à supporter. On écrase alors les petits vides de maçonnerie et on fait fléchir les poutres lors de leur installation.

Traditionnellement, les travaux de reprises en sous-œuvre ordinaires se passent de cette étape.

L’entreprise Debord, spécialisée dans les reprises en sous-œuvre, réalise systématiquement ces mises en charge.

### Éléments verticaux

{{< figure src="/images/comprendre/charge03.webp" class="w-30-ns w-50 fr">}}

Pour les éléments verticaux (poteaux, maçonneries), les problèmes de mise en charge sont essentiellement des phénomènes de tassement, au droit des liaisons, blocages ou scellements des éléments mis en place.

Les poteaux en eux-mêmes ne fléchissent pas sous la charge. Ou alors c’est qu’il y a un sérieux problème…

En effet, à l’inverse d’une poutre, la variation de hauteur d’un poteau est très faible en fonction de la charge qui lui est appliquée, et ce, qu'il soit en bois, en métal ou maçonné. Il commencera à flamber (c'est-à-dire à bomber, à plier) de façon importante bien avant de varier en hauteur.

Par contre, les interfaces de liaison au-dessus et en dessous de l’élément vertical pourront varier, par retrait des mortiers au cours de la prise, ou par écrasement des différentes couches existantes au moment où les interfaces « prennent leur place », lorsque la charge est appliquée.

Il suffit donc d’appliquer la bonne charge immédiatement sur le nouvel élément, au vérin par exemple, pour mettre tout en place immédiatement et atteindre l'équilibre souhaité.

### Éléments horizontaux

Les poutres, par contre, fléchissent en fonction de la charge, aboutissant à un affaissement proportionnel à cette charge et à la longueur de la poutre.

La mise en charge des poutres et solives doit donc arriver à compenser les affaissements futurs aux liaisons, mais surtout le fléchissement dû à la souplesse des éléments mis en œuvres. Il faut bien comprendre que tout élément porteur horizontal fléchira dès qu’on lui applique une charge, quel que soit le matériau (bois, acier ou béton).

C’est pour cette raison que l’on ressent la souplesse en sautant sur un plancher ou lorsqu’un camion passe sur un pont à côté de vous. C’est l’élément le plus contraignant dans les calculs de poutre dès que la portée est supérieure à 1 ou 2 mètres.

>Ce principe est utilisé dans certaines balances, avec des éléments métalliques à échelle plus petite qui fléchissent sous la charge, car la flèche est directement proportionnelle à la charge. La balance mesure en réalité la flèche dûe à l’élasticité de l’élément métallique.

Il existe des normes pour limiter cette flèche et éviter d’avoir l’impression d’habiter sur un trampoline. Par exemple, pour les planchers courants d’habitation, supportant des cloisonnements, il ne faut pas dépasser 1/500e de la portée. Concrètement, cela veut dire que pour un élément avec une portée de 5 mètres, la flèche maximale autorisée sera de 10 millimètres. À ceci s’ajoutent les différences de plusieurs millimètres ou centimètres d'écrasement que l’on risque de retrouver aux appuis et interfaces de liaison.

La mise en charge permettra d'équilibrer globalement ces différents affaissements.

### En résumé, pourquoi une mise en charge ?

Le but de la mise en charge est de contraindre l’élasticité des éléments lors de leur mise en place, pour leur faire prendre leur forme et leur place finales, avec une flèche qui forcera vers le haut les maçonneries qu’ils doivent supporter. On éliminera au passage les différents vides qui pourraient être amenés à se tasser sous la charge.

Si ce travail n’est pas réalisé, les maçonneries vont s’assoir lentement d’elles-mêmes et descendre au cours des mois ou années suivantes.

{{< figure src="/images/comprendre/solives.webp" title="Exemple d'une réalisation avec et sans mise en charge" >}}

L’absence de mise en charge provoquera rarement la ruine du bâtiment, mais engendrera progressivement de nombreuses fissures, déformations avec blocages des portes et fenêtres, d’abord juste au-dessus, puis successivement dans les étages supérieurs.

Avec le temps, cette évolution se stabilise en général si les renforts ont été correctement calculés, mais il arrive que ces déformations provoquent des désordres graves par rupture ou déchaussement de poutres au cours de la déformation lente du bâtiment.

<div class="imgandcaption">

{{< figure src="/images/comprendre/erreur00.webp" title="L'étaiement, ce n'est pas pour ça !" class="w-50-ns ml0 fl-ns">}}

On rencontre dans les reprises en sous-œuvre l'erreur qui consiste à utiliser un étai soulageant le centre de la poutre lors de son installation, comme sur le schéma. Mais c'est faire l’inverse de l’effet recherché, il faut au contraire appuyer au centre ou remonter les bords. La confusion vient de la nécessité d’étayer les coffrages des poutres et dalles en béton armé, avant le durcissement du béton.

</div>

### L'alternative du surdimensionnement et ses limites

Les bureaux d’étude qui ne peuvent compter sur des opérations de mise en charge (car très peu usitées par les entreprises) sont contraints de surdimensionner les poutres afin de minimiser les flèches futures.

Malheureusement, ce système montre vite ses limites. Si nous reprenons notre exemple d'une poutre avec une portée de 5 mètres et une flèche de 10 millimètres et qu'on la remplace par une poutre deux fois plus grosse et deux fois plus résistante, la flèche maximale sera réduite à 5 millimètres et la flèche effective sous charge moyenne de l’ordre de 3 millimètres :

- Il restera donc 3 millimètres de déformation, ce qui est toujours loin d’être négligeable en termes de fissuration des cloisons…
- La poutre sera de dimension et d’encombrement plus importants.
- La mise en œuvre sera plus coûteuse en raison du surdimensionnement et du surpoids.

## Le calcul

{{< figure src="/images/comprendre/charge08.webp" title="Une mise en charge efficace nécessite des calculs" class="w-50-ns mr0 fr-ns">}}

Afin de parfaitement compenser les charges en jeu, un calcul précis est nécessaire, ainsi qu'une mesure toute aussi précise lors de l’exécution de l'ouvrage.

Pour ce calcul, on étudiera les charges à reprendre, en décomposant les charges fixes – poids des matériaux – et les charges d’exploitation – toutes les charges qui peuvent être mises en place ou enlevées, comme les personnes, les meubles ou encore la neige et le vent. De cela, on déduira la force à appliquer dans le renfort mis en place. La précontrainte sera déterminée avec une force se situant entre les charges fixes brutes (sans pondération) et les charges complètes pondérées. Il existe une multitude de facteurs à prendre en compte, aussi toutes les données recueillies au départ sont cruciales. Ce n'est qu'avec précision et rigueur qu'on peut se rapprocher au mieux d'un chiffre exact qui aboutira à maintenir l’équilibre et à éviter les fissurations.

Cette première étape de calcul est la clef de voûte de ce système.

## La mesure

Mais comment mesurer sur chantier la force appliquée ?

### Mesure au dynamomètre de pied

{{< figure src="/images/comprendre/charge003.webp" title="Un outil maison, conçu et réalisé par Debord SAS" class="w-50-ns mr0 mt0 fr-ns">}}

Afin d’améliorer et de simplifier les mises en charge, nous avons développé un appareil de mesure de la force avec lecture directe en décanewtons (daN) ou en kilogrammes.

Cet appareil, à l’image d’un pèse personne, permet de connaître avec précision la charge que reprend un étai. Nous mesurons directement les efforts lors des mises en charge. Il permet aussi de connaitre la variation de charge, en cas de surveillance d’un bâtiment, pour contrôler si un étaiement prend de la charge ou non (en raison de la viscosité du bâtiment). Il est d’une utilisation simple et aisée sur chantier.

### La mesure de flèche

{{< figure src="/images/comprendre/charge001.webp" title="On peut mesurer la flèche d'une poutrelle" >}}

Le profil de nos poutrelles et leurs caractéristiques étant parfaitement connus, nous pouvons calculer avec précision la force appliquée en fonction du fléchissement de la poutre que l’on peut mesurer au pied à coulisse, au mètre ou au télémètre.

{{< figure src="/images/comprendre/charge011.webp" title="Proportionnalité entre la force et la flèche" class="w-50-ns center-ns">}}

## Aller plus loin : quelles techniques ?

C'est bien beau de savoir tout ça, mais en pratique, comment fait-on pour faire des mises en charge sur chantier ? Pour tout savoir sur les techniques de réalisation, [c'est par ici : les techniques de mise en charge](/comprendre/les-techniques-de-mise-en-charge) !
