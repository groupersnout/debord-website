---
title: "Le bois par le bois"
description: "Concernant les murs en pan de bois"
date: 2020-02-29T22:47:02+01:00
draft: true
featured_image: /images/realisations/real_vignette_boisbois_01.jpg
show_reading_time: true
toc: true
summary: "Il est souvent prévu le remplacement des sections de bois endommagées par des bois de même section initiale. Mais ce n'est pas toujours la meilleure chose à faire."
categories:
- poutre
- pan de bois
---

Il est souvent prévu le remplacement des sections de bois endommagées par des bois de même section initiale, avec quelque fois interdiction d’emploi de résine de renfort.

Cette disposition intellectuellement séduisante ne se justifie pas techniquement dans le cadre d’une réparation partielle.

En effet, la dépose systématique des bois endommagés, et leur remplacement par des bois de même section initiale, entraine une destruction majeure des hourdissages qui contribuent, avec les structures bois, au cheminement des descentes de charge. Nous obtenons finalement une déstabilisation majeur de la partie de mur porteur concernée, souvent avec affaissement supplémentaire qui se prolonge ensuite pendant quelques années. Nous préférons ne pas toucher à ces remplissages maçonnés en renforçant les bois existants par des compléments et renforts résine ou métalliques qui peuvent, en s’insérant dans les vides laissés par la pourriture des bois, apporter une beaucoup plus grande résistance que le bois d’origine et effectuer alors un confortement optimum sans déstabilisation des maçonneries, et des bois eux-mêmes pour les parties restant en charge.

Concernant les parties totalement dégradées dont aucune section de bois ne subsiste, nous préférons insérer une prothèse métallique qui s’insérera mieux dans les vides laissés sans avoir à dégager les bords, et pour une résistance supérieure. De plus, ces prothèses peuvent, pour les poteaux de fond principalement , être équipés de vérins de mise en charge permettant de remettre précisément à l’équilibre cette partie de mur. Notons que, contrairement aux coins enfoncés à force, les vérins sont mis à la charge exacte nécessaire, à la clé dynamométrique, alors que le coin ne fera pas la différence entre une force de 1 tonne ou de 10 tonnes que devra reprendre le poteau et déterminé par le calcul. (voir notre article sur les mise en charges)

{{< figure src="/images/realisations/real_croquis_boisbois_01.jpg" title="État intial">}}

{{< figure src="/images/realisations/real_croquis_boisbois_02.jpg" title="Remplacement en bois">}}

{{< figure src="/images/realisations/real_croquis_boisbois_03.jpg" title="Remplacement en prothèse métallique">}}
