---
title: "Documents"
description: ""
date: 2020-05-28T23:15:05+02:00
draft: false
featured_image: /images/lentreprise/
show_reading_time: false
toc: true
summary: ""
---

#### Vous trouverez ici diverses attestations qui pourraient vous être utiles (assurance, TVA…)


## Attestation de TVA

{{< button src="/pdf/1 - Attestation TVA + notice (ID 131148).pdf" msg="Attestation TVA et notice">}}

{{< button src="/pdf/Attestation TVA - Structure - pré-remplie (ID 121020).pdf" msg="Attestation TVA pré-remplie">}}

## Attestation d'assurance

{{< button src="/pdf/Attestation assurance DEBORD - 2020.pdf" msg="Attestation d'assurance">}}

## Attestation de vigilance

{{< button src="/pdf/SAS DEBORD - Attestation de vigilance URSSAF valable  31.08.2020 le 09.04.2020 (ID 285917).pdf" msg="Attestation de vigilance">}}
