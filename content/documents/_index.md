---
title: "Documents"
date: 2020-04-21T14:39:43+02:00
draft: false
weight: 3
noarticles: true
toc: true
---

#### Vous trouverez ici différents documents importants qui pourraient vous être utiles.

## Conditions générales

Pour consulter nos conditions générales d'exécution de prestation, rendez-vous sur [cette page](/documents/conditions-generales).

## Documents administratifs

### Extrait Kbis

- [Extrait Kbis au format pdf](/pdf/infogreffekbis.pdf)

### Attestations d'assurance

- [Attestation d'assurance 2022](/pdf/Attestation_assurance2022.pdf)

- [Attestation d'assurance 2021](/pdf/Attestation_assurance2021.pdf)

- [Attestation d'assurance 2020](/pdf/Attestation_assurance2020.pdf)

- [Attestation d'assurance 2019](/pdf/Attestation_assurance2019.pdf)

- [Attestation d'assurance 2018](/pdf/Attestation_assurance2018.pdf)

- [Attestation d'assurance 2017](/pdf/Attestation_assurance2017.pdf)

- [Attestation d'assurance 2016](/pdf/Attestation_assurance2016.pdf)

### Attestation de vigilance

- [Attestation de vigilance au format pdf](/pdf/Attestation_de_Vigilance_URSSAF.pdf)

### Salariés étrangers

- [Liste des salariés étrangers au format pdf](/pdf/Salaries_etrangers.pdf)

### Attestation de régularité fiscale

- [Attestation de régularité fiscale au format pdf](/pdf/Attestation_régularité_fiscale.pdf)

### RIB

- [RIB au format pdf](/pdf/RIB-DebordSAS.pdf)
