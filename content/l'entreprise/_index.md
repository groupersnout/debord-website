---
title: "L'entreprise"
date: 2020-04-21T14:39:43+02:00
draft: false
weight: 1
noarticles: true
toc: true
---
## Nous sommes spécialisés dans la reprise de structures

{{< figure src="/images/Actuellement.webp">}}

L’entreprise maîtrise les techniques de reprises liées :

- aux diverses maçonneries traditionnelles
- au béton armé
- à la charpente bois
- à la charpente métallique
- aux résines et renforts en carbone

Connaissant bien les caractéristiques mécaniques de chaque matériau et les contraintes de mise en œuvre, l'entreprise Debord peut utiliser ces différentes techniques sur un même chantier en fonction de **LA** technique la plus appropriée en chaque point : la plus solide, la plus pérenne, la plus discrète…. et toujours économiquement justifiée.

Chaque élément existant est vérifié et recalculé. S’il doit être renforcé, le confortement sera défini et justifié par une **note de calcul**.

Face à un problème de structure, il est difficile pour un client (maître d’ouvrage) de savoir s’il doit s’adresser à un maçon, à un charpentier bois, à un métallier ou à un spécialiste en résine. Chacun pourra proposer sa technique mais il sera difficile de les solliciter et de les comparer, notamment sur l'aspect des garanties, des « dégâts collatéraux » ou encore l'impact esthétique.

>Il faut savoir que l’on doit une garantie décennale sur nos reprises de structures, mais avouons que 10 ans, ce n’est pas grand-chose sur la vie d’un bâtiment ! Surtout à Paris où les immeubles peuvent avoir plus de 40 fois cet âge… **C'est pourquoi nos reprises sont calculées pour une tenue « à vie ».**

## Debord, une enteprise centenaire

### Les débuts

L’entreprise Debord est née historiquement du phénomène migratoire qui a marqué le bâtiment avec la « montée à Paris » des ouvriers spécialisés comme les maçons, charpentiers et couvreurs.

{{< figure src="/images/Rivoli.webp" title="Maquette de la construction d'un bâtiment parisien. (Musée des arts et métiers)" >}}

Travaillant 10 ou 11 mois par ans à la capitale, jusqu’à 70 heures par semaine, ils revenaient l’hiver dans leur terre natale. Ces maçons se sont imposés comme une référence, notamment lors des grands travaux de Paris, sous Haussmann par exemple. Ils ont créé la réputation des **maçons de la Creuse**, instaurant des techniques de construction rigoureuses et transmettant un savoir-faire très pointu.

L’entreprise Debord, créée sous ce nom en 1929, était constituée de maçons creusois et résulte de la transmission de différentes entreprises qui portaient chacune le nom de leur dirigeant, en général maître-ouvrier maçon devenu entrepreneur (Chapouteau, Thévenin, Debord, etc). D'abord situés dans le 16e arrondissement de Paris, au 78bis rue du Théâtre, les locaux déménagent au 66 rue Boileau avant de s'implanter à Boulogne en 1960.

Parmi les faits marquants dans l'histoire de l’entreprise, on peut citer sa participation à la construction du [pont Alexandre III](/lentreprise/lepontalexandre3), inauguré à l’occasion de l’exposition universelle de 1900.

### Le cheminement

Depuis **1870**, l‘entreprise de maçonnerie à toujours travaillé sur Paris. Malheureusement, il ne nous reste que peu de documents du 19e siècle.

En **1929**, l’entreprise Debord construit des immeubles parisiens, sous la direction de **Marcel Debord**, qui l’inscrit parmi les entreprises de maçonnerie dites sérieuses, « avec pignon sur rue ». Elle participe ainsi à la reconstruction après la guerre en 1945.

En **1960**, avec **Robert Chantron**, elle se spécialise dans les travaux d’entretien et de préservation des maçonneries de ces immeubles. Ingénieur ETP, Robert Chantron a travaillé tout particulièrement les problèmes de pathologie de ces constructions parfois fragiles et riches en techniques diverses.

Il développa les méthodes de recherche systématiques des causes de désordre et mit au point les techniques d’intervention en milieu habité encore en vigueur actuellement.

En **1989**, sollicitée pour sa fiabilité, l'entreprise développe le réagencement tous corps d’état des appartements et locaux de ces bâtiments avec **Pierre-Yves Chantron**, de l’école Eyrolles. En plus de la maçonnerie, elle maîtrise alors les techniques d’agencement, de charpente, de couverture, plomberie, menuiserie, électricité, peinture…

La formation acquise par nos équipes pendant cette période permet à l’entreprise de maîtriser elle-même ou avec des partenaires les interventions annexes, ce qui permet maintenant de s’insérer sans heurts parmi les autres entreprises intervenant sur nos projets, comprenant parfaitement leurs problématiques. Cette expérience permet en amont, dès les premières études de renforcement, de tenir compte des techniques et des contraintes des différents corps d’état qui devront nous succéder.

En **1997**, la spécialisation **reprise de structure** s’officialise avec la formation de deux équipes de maître-ouvriers, dirigées par **Lionel Robert** et **Franck Boulin**, et la création d’un second poste d’ingénieur structure avec **Patrick Chantron**.

En **2020**, les équipes se sont encore renforcées : trois ingénieurs conçoivent et suivent maintenant nos opérations de confortement. 

