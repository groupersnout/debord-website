---
title: "Recrutement"
date: 2022-02-22T16:30:17+01:00
draft: false
weight: 4
---

{{< figure src="/images/recrutement.webp" class="center">}}


<div class="tc">

Nos techniques vous intéressent ?

Vous êtes passionnés par les vieilles pierres  et les structures de bâtiment ?

Vous aimez les problèmes de résistance des matériaux ?

Vous aimez la difficulté de petits chantiers complexes, méticuleux et en petite équipe ?

Vous n’avez pas peur de travailler dans l’embarras d’étais entouré de gens et de choses fragiles ?

</div>

## Rejoignez-nous !

#### Nous recherchons des « perles rares » en rénovation de charpente – métal – bois - maçonnerie

##### Maîtres-ouvriers
##### Apprentis
##### Conducteurs de travaux

{{< button src="/contact" msg="Prenez contact avec nous !">}}

####
