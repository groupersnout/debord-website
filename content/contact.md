---
title: "Contact"
date: 2020-04-02T23:18:05+02:00
draft: false
mapadresse: "//umap.openstreetmap.fr/fr/map/siege-social-debord_441625?scaleControl=false&miniMap=true&scrollWheelZoom=false&zoomControl=true&allowEdit=false&moreControl=false&searchControl=null&tilelayersControl=null&embedControl=null&datalayersControl=false&onLoadPanel=undefined&captionBar=false"
---

## Siège social

<div class="flex flex-column flex-row-l justify-between items-center">

  <div class="w-40-l onecontact pa4">

##### 1, rue Henri Martin
##### 92100 Boulogne

[debord@debord.fr](mailto:debord@debord.fr)

[01 46 20 51 59](tel:0146205159)

  </div>
  <div class="w-60-l">

{{< figure src="/images/Actuellement.webp" class="">}}

  </div>
</div>

## Autres contacts

<div class="fourcontacts">

<div class="onecontact center">
<img src="/images/PY.webp" class="photocontact">
<div class="contacttext">

### Pierre-Yves

[py@debord.fr](mailto:py@debord.fr)

[06 11 58 39 99](tel:0611583999)

</div>
</div>
<div class="onecontact center">
<img src="/images/PK.webp" class="photocontact">
<div class="contacttext">

### Patrick

[pk@debord.fr](mailto:pk@debord.fr)

[06 03 00 48 02](tel:0603004802)

</div>
</div>
<div class="onecontact center">
<img src="/images/Lionel.webp" class="photocontact">
<div class="contacttext">

### Lionel

[lionel@debord.fr](mailto:lionel@debord.fr)

</div>
</div>
<div class="onecontact center">
<img src="/images/Frank.webp" class="photocontact">
<div class="contacttext">

### Franck

[franck@debord.fr](mailto:franck@debord.fr)

</div>
</div>
<div class="onecontact center">
<img src="/images/Arnaud.webp" class="photocontact">
<div class="contacttext">

### Arnaud

[arnaud@debord.fr](mailto:arnaud@debord.fr)

</div>
</div>
<div class="onecontact center">
<img src="/images/Wassim.webp" class="photocontact">
<div class="contacttext">

### Wassim

</div>
</div>
</div>

## Accès personnel

Pour le personnel de l'entreprise Debord, [accès à l'ancien site](http://www.debord.fr/debordv1/)
